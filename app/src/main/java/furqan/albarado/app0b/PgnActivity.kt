package furqan.albarado.app0b

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_pgn.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class PgnActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var panganAdapter : AdapterDataPgn
    var daftarPangan = mutableListOf<HashMap<String,String>>()
    var idPangan = ""
    val mainUrl = "http://192.168.43.170/www/hewan/"
    val url = mainUrl+"show_pangan.php"
    val url2 = mainUrl+"query_pangan.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                queryPangan("insert")
            }
            R.id.btnUpdate ->{
                queryPangan("update")
            }
            R.id.btnDelete ->{
                queryPangan("delete")
            }
            R.id.btnFind -> {
                showDataPangan(edPangan.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pgn)
        panganAdapter = AdapterDataPgn(daftarPangan,this)
        lsPangan.layoutManager = LinearLayoutManager(this)
        lsPangan.adapter = panganAdapter
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    fun showDataPangan(jenisPangan : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPangan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var pgn = HashMap<String,String>()
                    pgn.put("id_pangan",jsonObject.getString("id_pangan"))
                    pgn.put("jenis_pangan",jsonObject.getString("jenis_pangan"))
                    pgn.put("merek",jsonObject.getString("merek"))
                    daftarPangan.add(pgn)
                }
                panganAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val pgn = HashMap<String, String>()
                pgn.put("jenis_pangan",jenisPangan)
                return pgn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryPangan(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataPangan("")
                    clearInputPangan()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val pgn = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        pgn.put("mode","update")
                        pgn.put("id_pangan",idPangan)
                        pgn.put("jenis_pangan",edPangan.text.toString())
                        pgn.put("merek",edMerek.text.toString())
                    }
                    "insert" -> {
                        pgn.put("mode","insert")
                        pgn.put("jenis_pangan",edPangan.text.toString())
                        pgn.put("merek",edMerek.text.toString())
                    }
                    "delete" -> {
                        pgn.put("mode","delete")
                        pgn.put("id_pangan",idPangan)
                    }
                }
                return pgn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataPangan("")
    }

    fun clearInputPangan(){
        idPangan = ""
        edPangan.setText("")
        edMerek.setText("")
    }
}