package furqan.albarado.app0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_hwn.*

class AdapterDataHwn(val dataHwn : List<HashMap<String,String>>, val hwnActivity: HwnActivity) :
    RecyclerView.Adapter<AdapterDataHwn.HolderDataHwn>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterDataHwn.HolderDataHwn {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_hwn,p0,false)
        return  HolderDataHwn(v)
    }

    override fun getItemCount(): Int {
        return dataHwn.size
    }

    override fun onBindViewHolder(p0: AdapterDataHwn.HolderDataHwn, p1: Int) {
        val data = dataHwn.get(p1)
        p0.txKode.setText(data.get("kd_hewan"))
        p0.txJenisHwn.setText(data.get("jenis_hewan"))
        p0.txJenisPgn.setText(data.get("jenis_pangan"))
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener({
            val pos = hwnActivity.daftarPangan.indexOf(data.get("jenis_hewan"))
            hwnActivity.spinPangan.setSelection(pos)
            hwnActivity.edKode.setText(data.get("kd_hewan"))
            hwnActivity.edJenisHwn.setText(data.get("jenis_hewan"))
            Picasso.get().load(data.get("url")).into(hwnActivity.imUpload)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }

    class HolderDataHwn(v: View) : RecyclerView.ViewHolder(v){
        val txKode = v.findViewById<TextView>(R.id.txKode)
        val txJenisHwn = v.findViewById<TextView>(R.id.txJnsHwn)
        val txJenisPgn = v.findViewById<TextView>(R.id.txPgnHwn)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}