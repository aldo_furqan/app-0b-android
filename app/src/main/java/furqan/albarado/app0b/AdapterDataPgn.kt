package furqan.albarado.app0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_pgn.*

class AdapterDataPgn(val dataPangan : List<HashMap<String,String>>, val pgnActivity: PgnActivity) :
    RecyclerView.Adapter<AdapterDataPgn.HolderDataPangan>(){

    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterDataPgn.HolderDataPangan {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_pgn,p0,false)
        return  HolderDataPangan(v)
    }

    override fun getItemCount(): Int {
        return dataPangan.size
    }

    override fun onBindViewHolder(holder: AdapterDataPgn.HolderDataPangan, position: Int) {
        val data = dataPangan.get(position)
        holder.txPangan.setText(data.get("jenis_pangan"))
        holder.txMerek.setText(data.get("merek"))
        if(position.rem(2) == 0) holder.cPangan.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cPangan.setBackgroundColor(Color.rgb(255,255,245))

        holder.cPangan.setOnClickListener({
            pgnActivity.idPangan = data.get("id_pangan").toString()
            pgnActivity.edPangan.setText(data.get("jenis_pangan"))
            pgnActivity.edMerek.setText(data.get("merek"))
        })
    }

    class HolderDataPangan(v: View) : RecyclerView.ViewHolder(v) {
        val txPangan = v.findViewById<TextView>(R.id.txtPangan)
        val txMerek = v.findViewById<TextView>(R.id.txtMerek)
        val cPangan = v.findViewById<ConstraintLayout>(R.id.cPangan)
    }
}