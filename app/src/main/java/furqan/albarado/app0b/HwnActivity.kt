package furqan.albarado.app0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_hwn.*
import kotlinx.android.synthetic.main.activity_hwn.btnDelete
import kotlinx.android.synthetic.main.activity_hwn.btnFind
import kotlinx.android.synthetic.main.activity_hwn.btnInsert
import kotlinx.android.synthetic.main.activity_hwn.btnUpdate
import kotlinx.android.synthetic.main.activity_pgn.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class HwnActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var hwnAdapter : AdapterDataHwn
    lateinit var panganAdapter : ArrayAdapter<String>
    var daftarHwn = mutableListOf<HashMap<String,String>>()
    var daftarPangan = mutableListOf<String>()
    val mainUrl = "http://192.168.43.170/www/hewan/"
    val url = mainUrl+"show_data.php"
    var url2 = mainUrl+"show_pangan.php"
    var url3 = mainUrl+"query_upd_del_ins.php"
    var imStr = ""
    var pilihPangan = ""
    var idHwn = ""
    var namafile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hwn)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        hwnAdapter = AdapterDataHwn(daftarHwn,this)
        mediaHelper = MediaHelper(this)
        listHwn.layoutManager = LinearLayoutManager(this)
        listHwn.adapter = hwnAdapter

        panganAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarPangan)
        spinPangan.adapter = panganAdapter
        spinPangan.onItemSelectedListener = itemSelected

        imUpload2.setOnClickListener(this)
        imUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload2 -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.imUpload -> {
                requestPermission()
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnFind -> {
                showDataHwn(edJenisHwn.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinPangan.setSelection(0)
            pilihPangan = daftarPangan.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihPangan = daftarPangan.get(position)
        }
    }

    fun requestPermission() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(imUpload,fileUri)
                namafile = mediaHelper.getMyFileName()
            }

    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataHwn("")
                    clearInputHwn()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hwn = HashMap<String,String>()
//                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
//                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hwn.put("mode","insert")
                        hwn.put("kd_hewan",edKode.text.toString())
                        hwn.put("jenis_hewan",edJenisHwn.text.toString())
                        hwn.put("image",imStr)
                        hwn.put("file",namafile)
                        hwn.put("jenis_pangan",pilihPangan)
                    }
                    "update" -> {
                        hwn.put("mode","update")
                        hwn.put("kd_hewan",edKode.text.toString())
                        hwn.put("jenis_hewan",edJenisHwn.text.toString())
                        hwn.put("image",imStr)
                        hwn.put("file",namafile)
                        hwn.put("jenis_pangan",pilihPangan)
                    }
                    "delete" -> {
                        hwn.put("mode","delete")
                        hwn.put("kd_hewan",edKode.text.toString())
                    }
                }
                return hwn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getJenisPangan(jenisPangan : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarPangan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarPangan.add(jsonObject.getString("jenis_pangan"))
                }
                panganAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hwn = HashMap<String, String>()
                hwn.put("jenis_pangan",jenisPangan)
                return hwn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataHwn(jenisHwn : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarHwn.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var hwn = HashMap<String,String>()
                    hwn.put("kd_hewan",jsonObject.getString("kd_hewan"))
                    hwn.put("jenis_hewan",jsonObject.getString("jenis_hewan"))
                    hwn.put("jenis_pangan",jsonObject.getString("jenis_pangan"))
                    hwn.put("url",jsonObject.getString("url"))
                    daftarHwn.add(hwn)
                }
                hwnAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hwn = HashMap<String, String>()
                hwn.put("jenis_hewan",jenisHwn)
                return hwn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataHwn("")
        getJenisPangan("")
    }

    fun clearInputHwn(){
        idHwn = ""
        edKode.setText("")
        edJenisHwn.setText("")
    }
}