package furqan.albarado.app0b

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnHwn.setOnClickListener(this)
        btnPgn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnHwn->{
                val show =  Intent(this,HwnActivity::class.java)
                startActivity(show)
            }
            R.id.btnPgn->{
                val show = Intent(this,PgnActivity::class.java)
                startActivity(show)
            }
        }
    }
}
